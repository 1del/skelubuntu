��    
      l      �       �   9   �   P   +     |  4   �  T   �  
   "  	   -     7     =  +  X  E   �  �   �     a  8   }  ^   �          "     0     5        
             	                        Filter "Log level 128" messages starting with these words Filter messages sent to ~/.xsession-errors, to avoid flooding and save your SSD. Sanitize ~/.xsession-errors Show the contents of ~/.xsession-errors in real time This extension has been disabled because it was no longer needed since Cinnamon 4.8. To filter? Word list Words ~/.xsession-errors monitor Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 
Last-Translator: Claudiux <claude.clerc@gmail.com>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n > 1);
 Filtrer les messages du type "Log level 128" commençant par ces mots Filtre les messages envoyés au fichier ~/.xsession-errors, afin d'éviter qu'il soit noyé de messages inutiles et afin d'épargner votre disque SSD. Assainir ~/.xsession-errors Afficher en temps réel le contenu de ~/.xsession-errors Cette extension a été désactivée parce qu'elle n'est plus nécessaire depuis Cinnamon 4.8. À filtrer ? Liste de mots Mots Moniteur de ~/.xsession-errors 