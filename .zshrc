#!/usr/bin/env zsh
#   _________  _   _ ____   ____
#  |__  / ___|| | | |  _ \ / ___|
#    / /\___ \| |_| | |_) | |
# _ / /_ ___) |  _  |  _ <| |___
#(_)____|____/|_| |_|_| \_\\____|
#
USE_POWERLINE="true"
# Source zsh-configuration
if [[ -e /usr/share/zsh/zsh-config ]]; then
  source /usr/share/zsh/zsh-config
fi
# Use zsh prompt
if [[ -e /usr/share/zsh/zsh-prompt ]]; then
  source /usr/share/zsh/zsh-prompt
fi
